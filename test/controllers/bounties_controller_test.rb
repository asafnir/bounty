require 'test_helper'

class BountiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bounty = bounties(:one)
  end

  test "should get index" do
    get bounties_url
    assert_response :success
  end

  test "should get new" do
    get new_bounty_url
    assert_response :success
  end

  test "should create bounty" do
    assert_difference('Bounty.count') do
      post bounties_url, params: { bounty: { accepts: @bounty.accepts, available_for_token_sale: @bounty.available_for_token_sale, escrow: @bounty.escrow, fundraising_goal: @bounty.fundraising_goal, goal: @bounty.goal, ico_active_from: @bounty.ico_active_from, ico_token_price: @bounty.ico_token_price, instruction: @bounty.instruction, kyc: @bounty.kyc, no_of_team_members: @bounty.no_of_team_members, no_of_token: @bounty.no_of_token, prototypr: @bounty.prototypr, rewords_for: @bounty.rewords_for, role_of_token: @bounty.role_of_token, sale_starts: @bounty.sale_starts, short_explanation: @bounty.short_explanation, social_activiti_level: @bounty.social_activiti_level, string: @bounty.string, ticker: @bounty.ticker, toke_issue: @bounty.toke_issue, token_type: @bounty.token_type, total_tokens: @bounty.total_tokens, whitelist: @bounty.whitelist } }
    end

    assert_redirected_to bounty_url(Bounty.last)
  end

  test "should show bounty" do
    get bounty_url(@bounty)
    assert_response :success
  end

  test "should get edit" do
    get edit_bounty_url(@bounty)
    assert_response :success
  end

  test "should update bounty" do
    patch bounty_url(@bounty), params: { bounty: { accepts: @bounty.accepts, available_for_token_sale: @bounty.available_for_token_sale, escrow: @bounty.escrow, fundraising_goal: @bounty.fundraising_goal, goal: @bounty.goal, ico_active_from: @bounty.ico_active_from, ico_token_price: @bounty.ico_token_price, instruction: @bounty.instruction, kyc: @bounty.kyc, no_of_team_members: @bounty.no_of_team_members, no_of_token: @bounty.no_of_token, prototypr: @bounty.prototypr, rewords_for: @bounty.rewords_for, role_of_token: @bounty.role_of_token, sale_starts: @bounty.sale_starts, short_explanation: @bounty.short_explanation, social_activiti_level: @bounty.social_activiti_level, string: @bounty.string, ticker: @bounty.ticker, toke_issue: @bounty.toke_issue, token_type: @bounty.token_type, total_tokens: @bounty.total_tokens, whitelist: @bounty.whitelist } }
    assert_redirected_to bounty_url(@bounty)
  end

  test "should destroy bounty" do
    assert_difference('Bounty.count', -1) do
      delete bounty_url(@bounty)
    end

    assert_redirected_to bounties_url
  end
end
