task upcoming_ico: :environment do
	response = Net::HTTP.get_response(URI.parse('https://api.icowatchlist.com/public/v1/upcoming'))
  result = JSON.parse(response.body)
  unless result["ico"]["upcoming"].blank?
    result["ico"]["upcoming"].each do |ico|
    	if Bounty.where(name: ico["name"]).blank?
      	Bounty.create(name: ico["name"], image_url: ico["image"], short_explanation: ico["description"],start_time: ico["start_time"].to_date, end_time: ico["end_time"].to_date)
      end
    end
  end
end