class Participant
  include Mongoid::Document
  field :user_id, type: Integer
  field :bounty_id, type: Integer

  belongs_to :bounty
  belongs_to :user
end
