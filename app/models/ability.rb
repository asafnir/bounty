class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    # Define User abilities
    if user.roles? :admin
      can :manage, User  
    else
      can :read, Bounty
    end
  end
end
