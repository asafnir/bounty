class Bounty
  include Mongoid::Document
  include Mongoid::Attributes::Dynamic
  field :short_explanation, type: String
  field :rewords_for, type: String
  field :no_of_token, type: Integer
  field :instruction, type: String
  field :sale_starts, type: Date
  field :goal, type: Integer
  field :ticker, type: String
  field :token_type, type: String
  field :ico_token_price, type: Integer
  field :fundraising_goal, type: Integer
  field :total_tokens, type: Integer
  field :available_for_token_sale, type: Float
  field :whitelist, type: Mongoid::Boolean
  field :kyc, type: Mongoid::Boolean
  field :toke_issue, type: String
  field :string, type: String
  field :accepts, type: String
  field :social_activiti_level, type: String
  field :no_of_team_members, type: Integer
  field :ico_active_from, type: Date
  field :prototypr, type: String
  field :role_of_token, type: String
  field :escrow, type: Mongoid::Boolean
  field :user_id, type:String
  field :organizer_id, type:String
  field :status, type:String, default: 'pending'
  field :name, type:String
  field :start_time, type:Date
  field :end_time, type:Date 
  field :image_url, type:String
  # fiel

  has_many :participants
  has_many :comments

  def users
    User.in(id: participants.map(&:user_id))
  end

end
