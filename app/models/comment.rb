class Comment
  include Mongoid::Document
  include Mongoid::Timestamps::Created
  include Mongoid::Timestamps::Updated
  
  field :body, type: String
  field :user_id, type: String
  field :bounty_id, type: String

  belongs_to :user
  belongs_to :bounty
end
