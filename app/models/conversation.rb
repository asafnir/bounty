class Conversation 
  include Mongoid::Document

  field :recipient_id, type: String 
  field :sender_id, type: String


  has_many :messages, dependent: :destroy
  # belongs_to :sender, foreign_key: :sender_id, class_name: User
  # belongs_to :recipient, foreign_key: :recipient_id, class_name: User

  # validates :sender_id, uniqueness: { scope: :recipient_id }

  scope :between, -> (sender_id, recipient_id) do
    a = where(sender_id: sender_id, recipient_id: recipient_id) 
    b = where(sender_id: recipient_id, recipient_id: sender_id) 
    if a.blank?
      b
    else
      a
    end
  end
  # scope :between, -> (sender_id,recipient_id) do
  #   where("(conversations.sender_id = ? AND conversations.recipient_id =?) OR (conversations.sender_id = ? AND conversations.recipient_id =?)", sender_id,recipient_id, recipient_id, sender_id)
  # end

  def self.get(sender_id, recipient_id)
    conversation = between(sender_id, recipient_id).first
    return conversation if conversation.present?
    create(sender_id: sender_id, recipient_id: recipient_id) unless recipient_id.blank?
  end

  def opposed_user(user)    
    if self.recipient_id==user.id.to_s
      User.find(self.sender_id)
    else
      User.find(self.recipient_id)
    end
  end
end