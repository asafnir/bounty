class User
  include Mongoid::Document
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  include SimpleEnum::Mongoid

  as_enum :roles, admin: 1, organizer: 2, participant: 3

  ## Database authenticatable
  field :email,              type: String, default: ""
  field :encrypted_password, type: String, default: ""

  ## Recoverable
  field :reset_password_token,   type: String
  field :reset_password_sent_at, type: Time

  ## Rememberable
  field :remember_created_at, type: Time

  ## Trackable
  field :sign_in_count,      type: Integer, default: 0
  field :current_sign_in_at, type: Time
  field :last_sign_in_at,    type: Time
  field :current_sign_in_ip, type: String
  field :last_sign_in_ip,    type: String
  field :roles,              type: Integer
  field :eth_address,         type: String
  field :bitcoin_talk_profile_url,         type: String
  field :telegram_id,         type: Integer

  field :project_name,        type: String
  field :token_abbreviation,  type: String
  field :website,             type: String
  field :twitter,         type: String
  field :medium,         type: String
  field :facebook,         type: String
  field :slack,         type: String
  field :ico,         type: String
  field :supply,         type: String
  field :total_supply,         type: Integer
  field :accept_currency,         type: String
  field :ico_start_time,         type: Date
  field :ico_end_time,         type: Date
  field :project_introduction,         type: String
  field :banner_template,     type: String
  field :status, type:String, default: 'pending'

  ## Confirmable
  # field :confirmation_token,   type: String
  # field :confirmed_at,         type: Time
  # field :confirmation_sent_at, type: Time
  # field :unconfirmed_email,    type: String # Only if using reconfirmable

  ## Lockable
  # field :failed_attempts, type: Integer, default: 0 # Only if lock strategy is :failed_attempts
  # field :unlock_token,    type: String # Only if unlock strategy is :email or :both
  # field :locked_at,       type: Time

  

  def is?( requested_role )
    self.roles == requested_role.to_s
  end

  has_many :participants
  has_many :messages
  has_many :conversations, :foreign_key => :sender_id

  has_many :comments

  def bounties
    Bounty.in(id: participants.map(&:bounty_id))
  end

  def will_save_change_to_email?
  end
end
