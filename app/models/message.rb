class Message
  include Mongoid::Document
  field :body, type: String
  field  :created_at, type: Date, default: Time.now
  field :conversation_id, type: String
  field :read, type: Boolean
  field :user_id, type: String
  belongs_to :user
  belongs_to :conversation
 
  def message_time
    created_at.strftime("%m/%d/%y at %l:%M %p")
  end

  #after_create { MessageBroadcastJob.perform_later(self.id.to_s) }
end
