class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        if @user.roles == :organizer
          notice_to_show = 'we get your details and we will come back to you soon and there is a priority if you donate X BTC.'
        else
          notice_to_show = 'User is successfully created'          
        end
        format.html { redirect_to @user, notice: notice_to_show }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    params[:user]["roles"] = params[:user]["roles"].to_i unless params[:user]["roles"].blank?
    respond_to do |format|
      if @user.update(user_params)
        if @user.status != "pending"
          notice_to_show = 'we get your details and we will come back to you soon and there is a priority if you donate X BTC.'
        else
          notice_to_show = 'User was successfully updated.'
        end
        format.html { redirect_to @user, notice: notice_to_show }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def create_profile
  end

  def upload_template_header
  
  end

  def upload_template
    unless current_user.nil?
      if params.has_key?("file")
        if params[:file].content_type == "text/html"
          params.permit!
          @content = params[:file].read
          current_user.banner_template = @content
          if current_user.save
            @error = ""
          else
            @error = "Some thing went wrong"
          end
        end
      end
    end
    content = params[:file].read
    respond_to do |format|
      format.js
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id]) 
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:eth_address, :bitcoin_talk_profile_url, :telegram_id,:project_name,:token_abbreviation,:website,:medium,:facebook,:slack,:ico,:supply,:total_supply,:accept_currency,:ico_start_time,:ico_end_time,:project_introduction,:roles)
    end
end