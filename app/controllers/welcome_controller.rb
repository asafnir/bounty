class WelcomeController < ApplicationController
  def index
  	# @bounties = Bounty.where(status:"approved")
    @bounties = Bounty.where(status:"approved")
  end

  def show
    @comment = Comment.new
  	@bounty = Bounty.find(params[:id])
  end

  def participate
  	unless current_user.blank?
  		@flag = true
  		if verify_recaptcha(model: @user)
  			@bounty = Bounty.find(params[:id])
        Participant.create(user_id: current_user.id,bounty_id: @bounty.id)
  		else
  			@flag = false
  		end
  		respond_to do |format|
        	format.js {}
      	end
  	end
  end
end
