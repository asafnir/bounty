class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  def after_sign_in_path_for(resource)
  	if current_user.roles == :admin || current_user.roles == :organizer
	  	if current_user.roles == :organizer
        flash[:notice] = 'we get your details and we will come back to you soon and there is a priority if you donate X BTC.'
      end
      return '/dashboard'
	  else
	  	super
	  end
	end

	protected

  def configure_permitted_parameters
  	params[:user]["roles"] = params[:user]["roles"].to_i unless params[:user].blank?
    devise_parameter_sanitizer.permit(:sign_up, keys: [:eth_address,:bitcoin_talk_profile_url,:telegram_id,:project_name,:token_abbreviation,:website,:twitter,:medium,:facebook,:slack,:ico,:supply,:total_supply,:accept_currency,:ico_start_time,:ico_end_time,:project_introduction,:roles])
  end
end
