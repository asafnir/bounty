class AdminController < ApplicationController
  helper_method :resource_name, :resource, :devise_mapping, :resource_class
  before_action :authenticate_user!, :except => [:login]

  def resource_name
    :user
  end
 
  def resource
    @resource ||= User.new
  end

  def resource_class
    User
  end
 
  def devise_mapping
    @devise_mapping ||= Devise.mappings[:user]
  end


  def index
  	if current_user.roles == :admin
  		@bounties = Bounty.all
  	elsif current_user.roles == :organizer
  		@bounties = Bounty.where(organizer_id: current_user.id)
  	end
  end

  def login
  end

  def show
  end

  def change_status
    @bounty = Bounty.find(params[:id])
    unless @bounty.blank?
      respond_to do |format|
        if @bounty.update(status: params["status"])
          format.html { redirect_to @bounty, notice: 'Bounty was successfully updated.' }
          format.json { render :show, status: :updated, location: @bounty }
        else
          format.html { render :show }
          format.json { render json: @bounty.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  def change_user_status
    @user = User.find(params[:id])
    unless @user.blank?
      respond_to do |format|
        if @user.update(status: params["status"])
          format.html { redirect_to @user, notice: 'User was successfully updated.' }
          format.json { render :show, status: :updated, location: @user }
        else
          format.html { redirect_to @user, notice: @user.errors }
          format.json { render json: @user.errors, status: :unprocessable_entity }
        end
      end
    end
  end

end
