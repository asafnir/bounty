class BountiesController < ApplicationController
  before_action :set_bounty, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  # GET /bounties
  # GET /bounties.json
  def index
    if current_user.roles == :admin
      @bounties = Bounty.all
    elsif current_user.roles == :organizer
      @bounties = Bounty.where(organizer_id: current_user.id)
    end
  end

  # GET /bounties/1
  # GET /bounties/1.json
  def show
  end

  # GET /bounties/new
  def new
    @bounty = Bounty.new
  end

  # GET /bounties/1/edit
  def edit
  end

  # POST /bounties
  # POST /bounties.json
  def create
    params["bounty"] = params["bounty"].merge('user_id': params[:user_id])
    params["bounty"] = params["bounty"].merge('organizer_id': params[:user_id])
    @bounty = Bounty.new(bounty_params)

    respond_to do |format|
      if @bounty.save
        format.html { redirect_to @bounty, notice: 'Bounty was successfully created.' }
        format.json { render :show, status: :created, location: @bounty }
      else
        format.html { render :new }
        format.json { render json: @bounty.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bounties/1
  # PATCH/PUT /bounties/1.json
  def update
    respond_to do |format|
      if @bounty.update(bounty_params)
        format.html { redirect_to @bounty, notice: 'Bounty was successfully updated.' }
        format.json { render :show, status: :ok, location: @bounty }
      else
        format.html { render :edit }
        format.json { render json: @bounty.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bounties/1
  # DELETE /bounties/1.json
  def destroy
    @bounty.destroy
    respond_to do |format|
      format.html { redirect_to bounties_url, notice: 'Bounty was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bounty
      @bounty = Bounty.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bounty_params
      params.require(:bounty).permit(:short_explanation, :rewords_for, :no_of_token, :instruction, :sale_starts, :goal, :ticker, :token_type, :ico_token_price, :fundraising_goal, :total_tokens, :available_for_token_sale, :whitelist, :kyc, :toke_issue, :string, :accepts, :social_activiti_level, :no_of_team_members, :ico_active_from, :prototypr, :role_of_token, :escrow, :user_id,:organizer_id,:name)
    end
end
