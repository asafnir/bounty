class ChatController < ApplicationController
  def index
  	session[:conversations] ||= []
    if current_user.roles == :organizer
      @bounty = Bounty.find(params["bounty_id"])
      @users = @bounty.participants unless @bounty.blank?
      
    end
    @conversations =[]
    @conversation = Conversation.get(current_user.id.to_s, params[:user_id])
    redirect_to conversation_messages_path(@conversation)
    add_to_conversations unless conversated?
  end

  def show
  end

  def edit
  end

  def update
  end

  private
 
  def add_to_conversations
    session[:conversations] ||= []
    session[:conversations] << @conversation.id rescue nil
  end
 
  def conversated?
    session[:conversations].include?(@conversation.id) rescue nil
  end
end
