class ConversationsController < ApplicationController
  before_action :authenticate_user!

  def index
    @users = User.all
    @conversations = Conversation.all
  end

  def create
    if Conversation.between(params[:sender_id],params[:recipient_id]).present?
      @conversation = Conversation.between(params[:sender_id], params[:recipient_id]).first
    else
      @conversation = Conversation.create!(conversation_params)
    end
    # @conversation = Conversation.get(current_user.id.to_s, params[:user_id])
    # add_to_conversations unless conversated?
    redirect_to conversation_messages_path(@conversation)
    # respond_to do |format|
    #   format.js
    # end
  end

  def close
    @conversation = Conversation.find(params[:id])
 
    session[:conversations].delete(@conversation.id)
 
    respond_to do |format|
      format.js
    end
  end
 
  private

  def conversation_params
    params.permit(:sender_id, :recipient_id)
  end
 
  # def add_to_conversations
  #   session[:conversations] ||= []
  #   session[:conversations] << @conversation.id rescue nil
  # end
 
  # def conversated?
  #   session[:conversations].include?(@conversation.id) rescue nil
  # end

end
