module ApplicationHelper
	def user_avatar
		# get the email from URL-parameters or what have you and make lowercase
		email_address = current_user.email.downcase
		# create the md5 hash
		hash = Digest::MD5.hexdigest(email_address)
		# compile URL which can be used in https://www.gravatar.com/avatar/#{hash}"
		profile_img = 'https://www.gravatar.com/avatar/'+hash
	  if profile_img.blank?
	  	'login_logo.png'
	  else
	  	profile_img
	  end				
	end
end
