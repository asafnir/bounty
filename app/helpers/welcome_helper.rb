module WelcomeHelper

	def comment_user_avatar(user)
		email_address = user.email.downcase
		# create the md5 hash
		hash = Digest::MD5.hexdigest(email_address)
		# compile URL which can be used in https://www.gravatar.com/avatar/#{hash}"
		profile_img = 'https://www.gravatar.com/avatar/'+hash
	  if profile_img.blank?
	  	'login_logo.png'
	  else
	  	profile_img
	  end			
	end
end
