json.extract! user, :id, :eth_address, :bitcoin_talk_profile_url, :telegram_id, :created_at, :updated_at
json.url user_url(user, format: :json)
