Rails.application.routes.draw do
  resources :comments
  # get 'conversations/index'

  # get 'conversations/create'

  # get 'conversations/update'

  resources :conversations, only: [:create]

  get 'chat/index'

  get 'chat/show'

  get 'chat/edit'

  get 'chat/update'

  get 'admin/index'

  get 'admin/login'

  get 'admin/show'

  resources :bounties
  root 'welcome#index'

  get 'welcome/show'

  devise_for :users
  resources :products

  get '/admin' => 'admin#login'
  get '/dashboard' => 'admin#index'
  get '/change_status/:id' => 'admin#change_status'
  get '/change_user_status/:id' => 'admin#change_user_status'
  resources :users
  get '/bounty/:id' => 'welcome#show'
  post '/participate' => 'welcome#participate'
  get '/create_profile' => 'users#create_profile'
  post '/upload_template' => 'users#upload_template'
  get '/upload_template_header' => 'users#upload_template_header'
  # resources :conversations, only: [:create]
  resources :conversations, only: [:create] do
    member do
      post :close
    end
  end

  resources :conversations do
    resources :messages
  end
  # resources :conversations do 
  #   resources :messages, only: [:create]
  # end

  resources :comments, only: [:index, :new, :create]

  mount ActionCable.server => '/cable'

  #   require 'sidekiq/web'
  # mount Sidekiq::Web => '/sidekiq'


end